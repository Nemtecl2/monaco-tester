# Introduction

Cette application est un kit de démarrage, inspiré du [projet](https://github.com/maximegris/angular-electron) de maximegris.
Elle a été conçue pour les membres du groupe Git Harpon de façon à faciliter le développement des Proofs of Concept mais aussi de l'application en général.

Pour cela, nous avons utilisé :

- Angular v7.0.3
- Electron v3.0.8
- Electron Builder v20.28.1
- SCSS
- Bootstrap 4
- Le kit de démarrage de maximegris

# Prérequis

Pour assurer le fonctionnement de cette application depuis n'importe quel OS, il est nécessaire d'avoir installé Node.js.

Nous utiliserons aussi yarn.

### Installation de Node.js
**Windows et macOS**

Il suffit de se rendre sur le site de [Node.js](https://nodejs.org/en/download/).

**Linux**

```sudo apt install nodejs```

```sudo apt install npm```

⚠️ Ne faites jamais de ```sudo npm``` sous Linux et macOS ⚠️


### Installation de yarn

**Windows**

Lancer simplement la commande ```npm install -g yarn```

**Linux et macOS**

Il existe un problème de permissions sur Linux et macOS. Pour résoudre ce dernier, il existe deux solutions :

- Changer les permissions du répertoire /usr/local/
- Changer le préfixe de npm

La procédure à suivre est disponible [ici](https://www.youtube.com/watch?v=bxvybxYFq2o).

Une fois cela fait, lancer 
```npm install -g yarn```

## Ce qui a déjà été fait sur le projet

Comme expliqué plus haut, ce projet a été créé en utilisant le kit de démarrage de [maximegris](https://github.com/maximegris).
Voici les quelques modifications apportées au projet de base :
- Suppression du fichier `CHANGELOG.md`
- Modification du fichier `README.md`
- Suppression de lignes inutiles dans le fichier `package.json` (description, homepage, author, keywords)
- Modification des scripts du fichier `package.json` pour utiliser uniquement yarn
- Ajout d'une ligne `artifactName` dans `electron-builder.json` pour le nom de la release
- Ajout du package `eslint` et `eslint-plugin-angular`
- Création d'un fichier `.eslintrc.js`

## S'approprier le projet

Pour vous approprier le projet, il existe deux solutions : 

- Le clone (cf [Getting Started](#getting-started))
- Fork le répertoire depuis gitlab

Si vous avez choisi la première option, il vous faut alors lancer les commandes suivantes :


```git remote remove origin```

```git remote add origin https://lien-vers-votre-repo-distant.git```

```git add .```

```git commit -m "Initial commit"```

```git push -u origin master```

Pour la suite, on considère que votre projet s'intitule `mon-projet`.

Ensuite, il vous suffit de modifier les occurences de `angular-electron` par `mon-projet` et les occurences `AngularElectron` par `MonProjet`.

Ces occurences se trouvent dans les fichiers `angular.json`, `package.json`, `electron-builder.json`, `index.html`, `app.e2e-spec.ts` et `app.po.ts`.

## Getting Started

Cloner le répertoire

```git clone https://gitlab.com/Nemtecl/angular-electron.git```

Installer les dépendances

```yarn install```

## Structure du projet

|Fichier / Dossier|Description|
|--|--|
|`package.json`| Fichier contenant les dépendandes, les scripts et diverses informations sur le projet. |
|`angular.json`| Fichier de configuration pour Angular. |
|`main.ts`| Fichier d'entrée pour Electron. |
|`electron-builder.json`| Fichier contenant toutes les informations pour la création d'une release. |
|`node_modules/`| Dossier contenant les modules natifs et installés. |
|`e2e/`| Dossier contenant les fichiers de tests. |
|`release/`| Dossier contenant la release après génération. |
|`src/styles.scss`| Feuille de style globale. |
|`src/assets/`| Dossier contenant les fichiers de traduction. |
|`src/app/components/`| Dossier contenant les composants. |
|`src/app/app.module.ts`| Le fichier app.module.ts. Il faut y indiquer l'ensemble des modules/services à ajouter. Contient de base plusieurs services et modules utiles (`TranslateModule`, `TranslateLoader`, `HttpClientModule`, `ElectronService`, etc.) |
|`src/app/app-routing.module.ts`| Dossier pour le routing. Contient par défaut uniquement une route vers le composant `HomeComponent`. |
|`src/app/providers`| Dossier contenant les services. |
|`src/app/providers/electron.service.ts`| Service contenant des modules natifs à Node.js. |

⚠️ Il faut supprimer le dossier release avant d'en regénérer un autre ⚠️

## Commandes disponibles

Comme expliqué plus haut, nous avons utilisé le kit de démarrage. Le fichier package.json contenait des commandes très utiles, que nous avons donc utilisées.

|Commande|Description|
|--|--|
|`yarn start`| Exécution de l'application en mode développement avec hot reload. |
|`yarn ng:serve:web`| Exécution de l'application dans un navigateur. |
|`yarn build`| Build l'application. Les fichiers de build se trouvent dans le dossier /dist. |
|`yarn build:prod`| Build l'application avec Angular aot. Les fichiers de build se trouvent dans le dossier /dist. |
|`yarn electron:linux`| Sous Linux, build l'application et crée un `.AppImage`. |
|`yarn electron:windows`| Sous Windows, build l'application et crée un `.exe`, exécutable pour les systèmes en 32 et 64 bits. |
|`yarn electron:mac`|  Sous macOS, build l'application et crée un `.dmg` contenant le `.app`. |

## Ajouter un composant

```ng g c components/nom-du-composant```

## Ajouter un service

```ng g s providers/nom-du-service```

## Ajouter un package 

```yarn add [package]```

## Supprimer un package

```yarn remove [package]```

## Ajouter un module natif à Node.js

L'application étant lancée avec Electron, il est possible d'ajouter des modules natifs à Node.js.
Pour cela, il suffit de les ajouter au service ElectronService. Plusieurs modules sont déjà disponibles (Child Process, File System, OS, etc.).
