import { browser, element, by } from 'protractor';

/* tslint:disable */
export class MonacoTesterPage {
  navigateTo(route: string) {
    return browser.get(route);
  }
}
