import { MonacoTesterPage } from './app.po';
import { browser, element, by } from 'protractor';

describe('monaco-tester App', () => {
  let page: MonacoTesterPage;

  beforeEach(() => {
    page = new MonacoTesterPage();
  });

  it('should display message saying App works !', () => {
    page.navigateTo('/');
    expect(element(by.css('app-home h1')).getText()).toMatch('App works !');
  });
});
